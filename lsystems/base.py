class Cmd(object):
    def __init__(self, name, param):
        self.name = name
        self.param = param

    def __repr__(self):
        return "%s(%s)" %(self.name, self.param)

class Rule(object):
    def match(self, cmd):
        pass
    def apply(self, cmd):
        pass

class RuleMgr(object):
    def __init__(self, rules):
        self.rules = rules

    def apply(self, start_cmds, nb):
        cmds = start_cmds
        for i in range(nb):
            cmds = self.apply_rules( cmds )
        return cmds

    def apply_rules(self, cmds):
        result = []
        for cmd in cmds:
            result.extend( self.apply_rule( cmd ) )
        return result

    def apply_rule(self, cmd):
        for rule in self.rules:
            if rule.match(cmd):
                return rule.apply(cmd)
        return [cmd]

class DrawCmd(object):

    def match(self, cmd):
        pass

    def execute(self, cmd, context): 
        pass

class Renderer(object):
    def __init__(self, drawing_cmds):
        self.drawing_cmds = drawing_cmds

    def draw(self, commands):
        context = self.get_context()
        self.pre_draw(context)
        for cmd in commands:
            for dc in self.drawing_cmds:
                if dc.match(cmd):
                    dc.execute(cmd, context)
        self.post_draw(context)

    def pre_draw(self, context):
        pass

    def post_draw(self, context):
        pass

    def get_context(self):
        return {}
