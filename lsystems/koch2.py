import base
import turtle
from math import cos, radians

def iso_tri(dist):
    angle = 60
    return [ 
             Left( angle ) ,
             Forward(dist) ,
             Right( angle*2 ) ,
             Forward(dist) ,
             Right( angle*2 ) ,
             Forward(dist) ,
             ]


class Forward(base.Cmd):
    def __init__(self, param):
        base.Cmd.__init__(self, 'forward', param)

class Left(base.Cmd):
    def __init__(self, param):
        base.Cmd.__init__(self, 'left', param)

class Right(base.Cmd):
    def __init__(self, param):
        base.Cmd.__init__(self, 'right', param)

class SplitRule():
    def __init__(self, angle):
        self.angle = angle

    def match(self, cmd):
        return isinstance(cmd, Forward)
        return cmd.name == 'forward'

    def apply(self, cmd):
        length   = cmd.param
        dist     = length / 3.0
        tri_side = (dist/2.0)/cos(radians(self.angle))
        #==
        result   = []
        result.append( Forward ( dist )  )
        result.append( Left( self.angle ) )
        result.append( Forward( tri_side) )
        result.append( Right(self.angle * 2 ) )
        result.append( Forward( tri_side ) )
        result.append( Left(self.angle ) )
        result.append( Forward( dist ) )
        return result

class DrawForward(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Forward)

    def execute(self, cmd, context):
        dist = cmd.param
        turtle.forward(dist)

class DrawLeft(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Left)

    def execute(self, cmd, context):
        angle = cmd.param
        turtle.left(angle)

class DrawRight(base.DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Right)

    def execute(self, cmd, context):
        angle = cmd.param
        turtle.right(angle)

def turtle_fast():
    turtle.hideturtle()
    turtle.tracer(8,0)

default_renderer = base.Renderer( [ DrawLeft(),
                                DrawRight(), 
                                DrawForward() ])

def test( dist=200, angle=60, nb_iter=5):
    turtle.reset()
    turtle_fast()
    start_cmd = iso_tri( dist )
    #==
    rulemgr = base.RuleMgr( [ SplitRule(angle) ])
    cmds =  rulemgr.apply( start_cmd, nb_iter)
    #==
    renderer = base.Renderer( [ DrawLeft(),
                                DrawRight(), 
                                DrawForward() ])
    renderer.draw(cmds)
    turtle.update()
