from base import DrawCmd, Renderer, RuleMgr
from koch2 import Forward, Left, Right, iso_tri, SplitRule
import pyglet
from pyglet import gl 
from math import cos, sin, radians

class DrawForward(DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Forward)

    def execute(self, cmd, context):
        dist = cmd.param
        current_vector = context['current_vector']
        current_pos    = context['points'][-1]
        vx = current_vector[0]
        vy = current_vector[1]
        #==
        x = current_pos[0]
        y = current_pos[1]
        #==
        new_pos = (x + vx*dist, y + vy*dist )
        context['points'].append( new_pos)

class DrawLeft(DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Left)

    def execute(self, cmd, context):
        angle                     = cmd.param
        current_vector            = context['current_vector']
        new_vector                = rotate(current_vector, angle)
        context['current_vector'] = new_vector

class DrawRight(DrawCmd):
    def match(self, cmd):
        return isinstance(cmd, Right)

    def execute(self, cmd, context):
        angle                     = cmd.param
        current_vector            = context['current_vector']
        new_vector                = rotate(current_vector, -angle)
        context['current_vector'] = new_vector

def rotate(vector, angle):
    #==
    x = vector[0]
    y = vector[1]
    #==
    theta = radians(angle);
    cs = cos(theta);
    sn = sin(theta);
    #==
    px = x * cs - y * sn; 
    py = x * sn + y * cs;
    new_vector = (px, py)
    return new_vector

class Window(Renderer, pyglet.window.Window):
    def __init__(self, drawing_cmds, **kw):
        pyglet.window.Window.__init__(self, **kw)
        self.renderer = GL_Renderer(drawing_cmds)

    def on_draw(self):
        self.clear()
        self.renderer.render()


class GL_Renderer(Renderer):
    def __init__(self, drawing_cmds):
        Renderer.__init__(self, drawing_cmds)
        self.points = []

    def post_draw(self, context):
        self.points = context['points'] 

    def get_context(self):
        return {'points':         [ (100,100) ],
                'current_vector': (1,0)
                }

    def render(self):
        print 'render'
        gl.glColor4f(1.0,0,0,1.0)
        gl.glEnable (gl.GL_LINE_SMOOTH);
        gl.glHint (gl.GL_LINE_SMOOTH_HINT, gl.GL_DONT_CARE)
        gl.glLineWidth (2)
        pts = []
        for (x,y) in self.points:
            pts.append(x)
            pts.append(y)
        print len(pts)
        pyglet.graphics.draw(len(self.points), pyglet.gl.GL_LINE_STRIP, ('v2f', tuple(pts)))

def test( dist=200, angle=60, nb_iter=5):
    start_cmd = iso_tri( dist )
    #==
    rulemgr = RuleMgr( [ SplitRule(angle) ])
    cmds =  rulemgr.apply( start_cmd, nb_iter)
    print len(cmds)
    #==
    drawing_cmds = [ DrawLeft(),
                     DrawRight(), 
                     DrawForward() ]
    window = Window(drawing_cmds)
    window.renderer.draw(cmds)
    pyglet.app.run()


if __name__ == "__main__":
    test()
