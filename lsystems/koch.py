import turtle
from math import cos, radians

def iso_tri(length):
    return [ ('l', 60), ('f', length), ('r', 120), ('f', length), ('r', 120), ('f', length) ]

def draw( commands):
    for c in commands:
        type, length = c
        if type == 'f':
            turtle.forward(length)
        elif type == 'l':
            turtle.left( length)
        elif type == 'r':
            turtle.right( length)

def repeat( nb, cmds, angle):
    result = cmds
    for i in range(nb):
        result = resplit( result, angle)
    return result

def resplit( cmds,angle ):
    result = []
    for c in cmds:
        type, length = c
        if type != 'f':
            result.append(c)
        else:
            new_cmds = split(length, angle)
            result.extend( new_cmds )
    return result

def split( length, angle):
    result = []
    dist = length / 3.0
    result.append( ( 'f', dist ) )
    result.append( ( 'l', angle ) )
    result.append( ( 'f', (dist/2.0)/cos(radians(angle)) ) )
    result.append( ( 'r', angle * 2 ) )
    result.append( ( 'f', (dist/2.0)/cos(radians(angle)) ) )
    result.append( ( 'l', angle ) )
    result.append( ( 'f', dist ) )
    return result

def setup():
    turtle.hideturle()
    turtle.delay(0)

def main():
    draw( repeat( 5, iso_tri( 2000 ), 60))

if __name__ == "__main__":
    main()

