from mesh import add_vertex
import string

def render_array(key,array):
    ll = []
    for el in array:
        ll.append(key)
        ll.append(' ')
        for comp in el:
            ll.append(str(comp))
            ll.append(' ')
        ll.append("\n")
    return "".join(ll)


def render_face_array(faces, vtx_offset):
    vn = []
    ll = []
    for f in faces:
        idx = add_vertex(vn, f.get_normal())
        ll.append('f ')
        for index in f.indices:
            ll.append(str(index+1+vtx_offset))
            ll.append('//')
            ll.append(str(idx+1+vtx_offset))
            ll.append(' ')
        ll.append("\n")
    return "".join(ll), vn

def render_shape(shape, mtl_mat, vtx_offset):
    #obj name
    o     = "o %s" %list(shape.faces)[0].source
    # write all vertices from shape
    v      = render_array('v', shape.vertices)
    f, vn  = render_face_array(shape.faces, vtx_offset)
    vn     = render_array("vn", vn)
    s      = "s 0"
    usemtl = "usemtl %s" %mtl_mat
    #===
    return '\n'.join( [ o, v, vn, usemtl, s, f ]), len(shape.vertices)

default_material = \
    """
    newmtl Material
    Ns 96.078431
    Ka 0.000000 0.000000 0.000000
    Kd 0.640000 0.640000 0.640000
    Ks 0.500000 0.500000 0.500000
    Ni 1.000000
    d 1.000000
    illum 2
    """

def material(name, r, g, b):
    data = """
           newmtl %s
           Ns 96.078431
           Ka 0.000000 0.000000 0.000000
           Kd %s %s %s
           Ks 0.500000 0.500000 0.500000
           Ni 1.000000
           d 1.000000
           illum 2
           """ %(name, r, g, b)
    return data



def write_file(shape, path):
    mtl_path = path.stripext()
    mtl_path = mtl_path + ".mtl"
    with open(path, 'w') as obj_file:
        mtllib = "mtllib %s \n" %mtl_path.name
        obj_file.write(mtllib)
        #==
        with open(mtl_path, 'w') as mat_file:
            vtx_offset = 0
            #==
            mat_name = shape.name + "material"
            mat_name = mat_name.translate(None, string.punctuation)
            mat_data = material( mat_name, *shape.faces[0].color[:3])
            mat_file.write( mat_data )
            #==
            obj_file.write( "# %s\n" %vtx_offset)
            s, new_vtx_offset  = render_shape( shape, mat_name, vtx_offset)
            obj_file.write( "# %s\n" %new_vtx_offset)
            vtx_offset += new_vtx_offset
            #==
            obj_file.write(s)
            obj_file.write("\n")
