from itertools import repeat, product

from vector import Vector
from color import Color
from math import sqrt
#import cgkit.cgtypes as cg

def Dodecahedron(radius, face_colors=None):
    '''
    Return a new Shape.
    One of the platonic solids.
    Verts are at the given `radius`.

    `face_colors` may either be a Color which is applied to every face, or
    a sequence of colors, one for each face.
    '''

    phi = (1 + sqrt(5)) / 2
    size = radius / sqrt(3) * phi
    b = size / phi
    c = size * (2 - phi)
    vertices = [
        (    0,  size,     c), # 0
        (    0,  size,    -c), # 1
        (    0, -size,     c), # 2
        (    0, -size,    -c), # 3

        ( size,     c,     0), # 4
        ( size,    -c,     0), # 5

        (    b,     b,     b), # 6
        (    b,     b, -   b), # 7
        (    b, -   b,     b), # 8
        (    b, -   b, -   b), # 9

        (    c,     0,  size), # 10
        (    c,     0, -size), # 11

        (-size,     c,     0), # 12
        (-size,    -c,     0), # 13

        (   -b,     b,     b), # 14
        (   -b,     b,    -b), # 15
        (   -b,    -b,     b), # 16
        (   -b,    -b,    -b), # 17

        (   -c,     0,  size), # 18
        (   -c,     0, -size), # 19
    ]
    faces = [
        [ 6,  0, 14, 18, 10],
        [16,  2,  8, 10, 18],
        [ 9,  3, 17, 19, 11],
        [15,  1,  7, 11, 19],
        [ 1,  0,  6,  4,  7],
        [ 0,  1, 15, 12, 14],
        [ 3,  2, 16, 13, 17],
        [ 2,  3,  9,  5,  8],
        [ 6, 10,  8,  5,  4],
        [ 9, 11,  7,  4,  5],
        [15, 19, 17, 13, 12],
        [16, 18, 14, 12, 13],
    ]
    return Shape(vertices, faces, face_colors, 'Dodecahedron')

def Cuboid(x, y, z, colors=None, source='Cuboid'):
    '''
    Return a new Shape, cuboid of dimensions x, y, z, centered on the origin.

    `colors` may be either an instance of Color, or a sequence of colors,
    one for each face.
    '''
    verts = list(product((-x/2, +x/2), (-y/2, +y/2), (-z/2, +z/2)))
    faces = [
        [0, 1, 3, 2], # left
        [4, 6, 7, 5], # right
        [7, 3, 1, 5], # front
        [0, 2, 6, 4], # back
        [3, 7, 6, 2], # +y top
        [1, 0, 4, 5], # -y bottom
    ]
    return Shape(verts, faces, colors, source)

def add_vertex(vertices, new_vert):
    '''
    Modifies `vertices` in-place by appending the given `new_vert`.
    Returns the index number of the new vertex.

    Loads of Shape-modifying algorithms seem to need this function. Can't make
    it a method because they often haven't constructed the shape instance yet.
    '''
    vertices.append(new_vert)
    return len(vertices) - 1


class Face(object):
    '''
    A single flat face that forms part of a Shape. Attributes are the params
    to the constructor below, plus:

        `normal`: A Vector, perpendicular to the face

    .. function:: __init__(indices, color, shape, source='unknown')

        `indices`: a list of int indices into the parent shape's vertex list

        `color`: an instance of Color

        `shape`: a reference to the parent Shape

        `source`: a descriptive string. These can be used when writing
            algorithms that modify shapes, to select certain faces to operate
            on.

    .. function:: __getitem__(n)

        Return the nth index, as an integer.

    .. function:: __iter__()

        Iterate through `indices`

    .. function:: __len__()

        Return the length of `indices`
    '''
    def __init__(self, indices, color, shape, source='unknown', up=None, visible=True):
        self.indices = indices
        self.color = color
        self.shape = shape
        self.source = source
        self.normal = self.get_normal() if len(indices) > 2 else None
        self.up     = up
        self.visible= True

    def __getitem__(self, index):
        return self.indices[index % len(self.indices)]

    def __iter__(self):
        return self.indices.__iter__()

    @property
    def vertices(self):
        return [self.shape.vertices[idx] for idx in self.indices]

    @property
    def items(self):
        return enumerate([self.shape.vertices[idx] for idx in self.indices])

    #def matrix(self):
        #return cg.mat4.lookAt( self.centroid, self.centroid+self.normal, self.up)

    #def matrix2(self):
        #return cg.mat4.lookAt( self.centroid, self.centroid+self.up, self.normal)

    #def local_vertices(self):
        #mm = self.matrix()
        #return [mm.inverse() * v for v in  self.vertices]

    def edges(self):
        edges    = []
        vertices = self.vertices()
        v1       = vertices[-1]
        for v2 in vertices:
            edges.append((v1, v2))
        return edges

    def __len__(self):
        return len(self.indices)

    def get_normal(self):
        '''
        Return the unit normal vector at right angles to this face.

        Note that the direction of the normal will be reversed if the
        face's winding is reversed.
        '''
        v0 = self.shape.vertices[self.indices[0]]
        v1 = self.shape.vertices[self.indices[1]]
        v2 = self.shape.vertices[self.indices[2]]
        a = v0 - v1
        b = v2 - v1
        return b.cross(a).normalized()

    @property
    def centroid(self):
        '''
        Warning: Not an accurate centroid, just the mean vertex position
        '''
        return sum(
            [self.shape.vertices[i] for i in self], Vector.origin
        ) / len(self.indices)

    @property
    def local_centroid(self):
        '''
        Warning: Not an accurate centroid, just the mean vertex position
        '''
        return sum(
            self.local_vertices(), Vector.origin
        ) / len(self.indices)


class Shape(object):
    '''
    Defines a polyhedron, a 3D shape with flat faces and straight edges.

    .. function:: __init__(vertices, faces, colors, name='unknown')

        `vertices`: a list of Vector points in 3D space, relative to the
        shape's center point.

        `faces`: a list of faces, where each face is a list of integer indices
        into the vertices list. The referenced vertices of a single
        face must form a coplanar ring defining the face's edges. Duplicate
        indices do not have to be given at the start and end of each face,
        the closed loop is implied.

        `colors`: a single Color which is applied to every face, or a sequence
        of colors, one for each face.

        `name`: the 'source' attribute to be applied to each face.

        See the source for factory functions like
        :func:`~gloopy.shapes.cube.Cube` for examples of constructing Shapes.
    '''
    def __init__(self, vertices, faces, colors, name='unknown', line_mode=False):

        # sanity checks
        len_verts = len(vertices)
        self.name = name
        for face in faces:
            assert len(face) >= 3 or line_mode
            for index in face:
                assert 0 <= index < len_verts

        # convert vertices from tuple to Vector if required
        if len(vertices) > 0 and not isinstance(vertices[0], Vector):
            #print vertices
            vertices = [Vector(*v) for v in vertices]

        # if color is a single color, then convert it to a sequence of
        # identical colors, one for each face
        if isinstance(colors, Color):
            colors = repeat(colors)
        else:
            colors = repeat(Color.Red)

        self.vertices = vertices
        self.faces = [
            Face(face, color, self, source=name)
            for face, color in zip(faces, colors)
        ]

    def __repr__(self):
        return '<Shape %d verts, %d faces>' % (
            len(self.vertices), len(self.faces),
        )

    #def map(self, matrix):
        #"""
        #matrix is a cgkit matrix
        #"""
        #self.vertices = [ Vector(*(matrix * v)) for v in self.vertices ]
        #for f in self.faces:
            #f.normal = f.get_normal()

    def get_edges(self):
        '''
        Return a set of pairs, each pair represents indices that start and end
        an edge. Contents of each pair is sorted. e.g Tetrahedron:
        { (0, 1), (1, 2), (0, 2), (0, 3), (1, 3), (2, 3), }
        '''
        edges = set()
        for face in self.faces:
            for i in xrange(len(face)):
                edges.add( tuple(sorted((face[i], face[i+1]))) )
        return edges


    def replace_face(self, index, new_faces):
        '''
        Replace the face at position 'index' in self.faces with the list of
        Face instances in 'new_faces'
        '''
        self.faces[index] = new_faces.pop()
        while new_faces:
            self.faces.append( new_faces.pop() )

