import numpy as np 
from PIL import Image
import itertools as it

w, h = 512,512
data =  np.zeros( (w,h,3), dtype=np.uint8)

data[[256,260],:] = [255,0,0]

img = Image.fromarray(data, 'RGB')
img.show()
img.save('my.png')

reds = [ (i,0,0) for i in range(0,255, 5)]
idx_color = it.izip( range(data.shape[0]), it.cycle( reds ))
for (idx, color) in idx_color:
    data[idx,...] = color
